// An aggregate is a cluster of things that have come or have been brought together.
// Aggregations are needed when your application needs a form of information that is not found.
/* 
   In MongoDB, 
   1. You would like to know which students have not finished their courses yet.
   2. Count the total number of courses a student has completed.
   3. Determine the number of students who completed a specified course.
   Agregation pipeline
   pass through pipeline
   
*/
// [SECTION]
/* 
   - Used to generate and manipulate data, and peform operations to create filtered results that helps us to analyze the data.



*/
//Using the aggregate method:
/* 
   - The "$match"
   - Syntax:
      {$match: {field: value}}
   - The "$group" is used to group elements with together and field-value pairs the data from the grouped elements.
   - Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stock for all suppliers found.
    -Syntax:
    {$group: {_id: "value"}, fieldResult: "valueResult"}}
    -Syntax 
      db.collectionName.aggregate([{$match:{fieldA:valueA}}, {$group:id_: "",fieldResult:"valueResult"}])
*/
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

//count,sum,average

//Field projection with aggregation
/* 
   -The "$project" can be used when aggregating data to include/exclude fields from the returned result.

   -Syntax: 
      {$project: {field: 1/0}}

*/
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// Sorting Aggregated results
/* 
   - The "$sort" can be used to change the order of the aggregated result.
   - Syntax:
      {$sort: {field:1/-1}}
   1  -> lowest to highest  - Ascending
   -1 -> highest to lowest  - Descending
*/
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating results based on array fields.

/* 
      - The "$unwind" deconstruct an array field from a collection/field with an array value to output a result.
      - Syntax:
      - {$unwind: field}
   */

db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);

// SECTION Other Aggragate stages

// $count all yellow fruits

db.fruits.aggregate([
  { $match: { color: "Yellow" } },
  { $count: "Yellow Fruits" },
]);

// $avg gets the average value of stock
db.fruits.aggregate([
  { $match: { color: "Yellow" } },
  { $group: { _id: "color", yellow_fruits_stock: { $avg: "$stock" } } },
]);

// $min & $max
db.fruits.aggregate([
  { $match: { color: "Yellow" } },
  { $group: { _id: "color", yellow_fruits_stock: { $min: "$stock" } } },
]);

db.fruits.aggregate([
  { $match: { color: "Yellow" } },
  { $group: { _id: "color", yellow_fruits_stock: { $max: "$stock" } } },
]);
